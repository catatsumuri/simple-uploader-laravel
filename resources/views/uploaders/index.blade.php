<x-uploader-layout>
  <div class="max-w-2xl mx-auto py-10 px-6 bg-white rounded-lg shadow-md">
    <h1 class="text-2xl font-semibold text-gray-700 mb-5">
      <a href="{{ route('uploaders.index') }}" class="hover:underline">Simple Uploader</a>
    </h1>
    <x-auth-session-status class="mb-4" :status="session('status')" />

    <form method="POST" action="{{ route('uploaders.store') }}" enctype="multipart/form-data" class="space-y-6">
      @csrf

      <div>
        <x-input-label for="file" :value="__('File')" class="block text-sm font-medium text-gray-700" />
        <div class="mt-1 flex items-center">
          <input type="file" id="file" class="block w-full text-sm text-gray-500
          file:mr-4 file:py-2 file:px-4
          file:rounded-full file:border-0
          file:text-sm file:font-semibold
          file:bg-violet-50 file:text-violet-700
          hover:file:bg-violet-100" name="file" required autofocus />
        </div>
        <x-input-error :messages="$errors->get('file')" class="mt-2" />
      </div>

      <div class="flex items-center justify-end mt-4">
        <x-primary-button>
          {{ __('Upload') }}
        </x-primary-button>
      </div>
    </form>
    <div class="mb-8">
      <h2 class="text-xl font-semibold text-gray-600 mb-3">Uploaded Files</h2>

      <div class="overflow-x-auto">
        <table class="min-w-full leading-normal">
          <thead>
            <tr>
              <th class="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
                {{ __('File Name') }}
              </th>
              <th class="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
                {{ __('Size') }}
              </th>
              <th class="px-5 py-3 border-b-2 border-gray-200 bg-gray-100"></th>
            </tr>
          </thead>
          <tbody>
            @forelse($uploadedFiles as $file)
              <tr>
                <td class="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                  <div class="flex items-center">
                    <div class="ml-3">
                      <p class="text-gray-900 whitespace-no-wrap">
                      <a href="{{ Storage::url('uploaded_files/'. $file->saved_name) }}" class="text-blue-600 hover:text-blue-900">{{ $file->saved_name }}</a>
                      </p>
                    </div>
                  </div>
                </td>
                <td class="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                  <p class="text-gray-900 whitespace-no-wrap">
                    {{ $file->size }}
                  </p>
                </td>

                <td class="px-5 py-5 border-b border-gray-200 bg-white text-sm text-right">
                  <form method="POST" action="{{ route('uploaders.destroy', $file->id) }}" onsubmit="return confirm('{{ __('Are you sure you want to delete this file?') }}')">
                    @csrf
                    @method('DELETE')
                    <x-danger-button>{{ __('Delete') }}</x-danger-button>
                  </form>
                </td>

              </tr>
            @empty
              <tr>
                <td colspan="3" class="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                  <p class="text-gray-900 whitespace-no-wrap text-center">
                    {{ __('No files uploaded yet.') }}
                  </p>
                </td>
              </tr>
            @endforelse
          </tbody>
        </table>
      </div>
    </div>
  </div>
</x-uploader-layout>
