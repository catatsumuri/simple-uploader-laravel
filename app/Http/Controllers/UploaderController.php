<?php

namespace App\Http\Controllers;

use App\Models\UploadedFile;
use Illuminate\Http\Request;

class UploaderController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $uploadedFiles = UploadedFile::latest()->get();
        return view('uploaders.index', ['uploadedFiles' => $uploadedFiles]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'file' => 'required|file',
        ]);
        $file = $request->file('file');

        $originalName = $file->getClientOriginalName();
        $mime = $file->getMimeType();
        $size = $file->getSize();
        $savedName = $savedName = \Str::random(10).md5($originalName);
        $data = [
            'original_name' => $originalName,
            'saved_name'    => $savedName,
            'mime_type'     => $mime,
            'size'          => $size,
        ];

        \DB::beginTransaction();
        $uploadedFile = UploadedFile::create($data);
        $extension = $file->getClientOriginalExtension();
        $savedName = sprintf('%05d.%s', $uploadedFile->id, $extension);
        $path = $file->storeAs('uploaded_files', $savedName, 'public');

        // saved_nameを更新
        $uploadedFile->update(['saved_name' => $savedName]);
        \DB::commit();

        // dd($request->all());
        return redirect(route('uploaders.index'))
            ->with(['status' => __('File uploaded')]);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(UploadedFile $uploader)
    {
        \Storage::delete('uploaded_files/' . $uploader->saved_name);
        $uploader->delete();
        return redirect()->route('uploaders.index')
                         ->with('status', __('File deleted successfully.'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(UploadedFile $uploadedFile)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(UploadedFile $uploadedFile)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, UploadedFile $uploadedFile)
    {
        //
    }

}
